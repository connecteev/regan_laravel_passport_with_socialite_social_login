const mix = require('laravel-mix');

mix.sass('resources/sass/app.scss', 'public/dist')
  .options({
    processCssUrls: false,
    autoprefixer: {
      options: {
        browsers: [
          'last 6 versions',
        ]
      }
    }
  })
  .react('resources/js/app.js', 'public/dist')
  .setPublicPath('public/dist');


// Compile JS using Babel when doing a production build
if (mix.inProduction()) {
  mix.babel('public/dist/app.js', 'public/dist/app.js');
}
