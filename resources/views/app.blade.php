<!DOCTYPE html>
{{--@if($caching)--}}
    {{--<html manifest="/manifest">--}}
{{--@else--}}
    <html>
{{--@endif--}}
<head>
    <title>{{ env('SITE_TITLE') }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <style>
        * {
            border: 0;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            outline: none;
            -webkit-appearance: none;
            vertical-align: top;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        body{
            background: #fff;
            width: 100%;
            padding:0;
            font-family:-apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        @-webkit-keyframes Loading {
            0%   { opacity: .1; }
            50% { opacity: 0.7; }
            100% { opacity: .1; }
        }
        @-moz-keyframes Loading {
            0%   { opacity: .1; }
            50% { opacity: 0.7; }
            100% { opacity: .1; }
        }
        @-o-keyframes Loading {
            0%   { opacity: .1; }
            50% { opacity: 0.7; }
            100% { opacity: .1; }
        }
        @keyframes Loading {
            0%   { opacity: .1; }
            50% { opacity: 0.7; }
            100% { opacity: .1; }
        }

        .Loading{
            content:'';
            position: fixed;
            top:50%;
            left: 50%;
            width: 260px;
            height: 260px;
            margin: -130px 0 0 -130px;
            background: url("{{env('LOGO_URL')}}") center center no-repeat;
            background-size: contain;
            animation: 3s ease Loading infinite;
        }
        @if(env('HEADER_LOGO_URL'))
        main:before{
            content:'';
            display: block;
            height: 50px;
            margin-bottom: 20px;
            background: url("{{env('HEADER_LOGO_URL')}}") 50% 0 no-repeat;
            background-size: auto 30px;
            border-bottom: 1px solid #eee;
        }
        @endif
    </style>

    <script>
        window.config = {!! $config !!};
    </script>

    @if(env('ANALYTICS_ID'))
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ env('ANALYTICS_ID') }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', "{{ env('ANALYTICS_ID') }}");
    </script>
    @endif
</head>
<body>
<div id="app"><div class="Loading"></div></div>

<link rel="stylesheet" href="/dist/app.css?v={{ env('LATEST_COMMIT') }}">
<script src="/dist/app.js?v={{ env('LATEST_COMMIT') }}"></script>
</body>
</html>
