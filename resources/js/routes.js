import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import Redirect from './containers/Redirect';
import Home from './containers/Home';
import Error from './containers/Error';
import Login from "./pages/Login";

class Routes extends React.Component {
    render() {
        if(this.props.appState.error){
            return <Error error={this.props.appState.error}/>
        }else {
            return (
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/error" component={Error} />
                    <Route exact path="/redirect/:redirect?" component={Redirect} />
                </Switch>
            )
        }
    }
}


function mapStateToProps(store) {
    return {
        appState: store.app.toJS()
    };
}

export default connect(mapStateToProps)(Routes);
