module.exports = {
    getConfig(id){

    },
    getBrowserType(){
        if(navigator.userAgent.match(/Android/i)){ return 'android'; }
        if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){ return 'ios'; }
        return 'web'
    },
    validate(str, type){
        let tests = {
            email : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            mobile : /^[\d+]{0,14}$/,
            name : /^[a-zA-Z\s]{1,40}$/,
            id : /^[a-zA-Z0-9]{1,40}$/
        };

        if(tests.hasOwnProperty(type)){
            let pattern = tests[type];
            return pattern.test(str)
        }

        return false;
    }
};