import React from 'react';
export default class extends React.Component {
    render() {
        return(
            <main className="Error">
                <h1>{window.config.settings.error.title}</h1>
                <div className="content">
                    <p>{this.props.error ? this.props.error : window.config.settings.error.generic_error }</p>
                </div>
            </main>
        );
    }
}