import React from 'react';
import {Link} from 'react-router-dom';
import Loading from '../pages/Loading';
import helpers from '../helpers';
export default class extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            attemptedRedirect : false
        }
    }

    componentWillMount(){
        const redirectConfig = this.getRedirectConfig();
        if(!redirectConfig){return;}
        const browserType = helpers.getBrowserType();
        setTimeout(()=>{
            this.setState({
                attemptedRedirect : true
            })
        }, 1000);

        if(browserType !== 'web'){
            if(redirectConfig[browserType+'AppNotInstalled']){
                setTimeout(function() {
                    location.href = redirectConfig[browserType+'AppNotInstalled']
                }, 500);
            }
            if(redirectConfig[browserType+'AppInstalled']){
                location.href = redirectConfig[browserType+'AppInstalled'];
            }
            return;
        }

        location.href = redirectConfig.webFallback;
    }

    getRedirectConfig(){
        const redirects = window.config.redirects;
        if(redirects && redirects.hasOwnProperty(this.props.match.params.redirect)){
            return redirects[this.props.match.params.redirect];
        }
        return false;
    }

    getStyles(image){
        if(image){
          return <style dangerouslySetInnerHTML={{__html: '.App {background-image: url('+image+')}'}} />
        }
        return null;
    }

    render() {
        const redirectConfig = this.getRedirectConfig();
        if(!redirectConfig.showAppStoreFallback || !this.state.attemptedRedirect){
            return <Loading loading={true}/>
        }
        return(
            <main className='Redirect'>
                <div className="appIcon ios web" style={{backgroundImage : 'url('+redirectConfig.iosAppIcon+')'}}></div>
                <div className="appIcon android" style={{backgroundImage : 'url('+redirectConfig.androidAppIcon+')'}}></div>
                <h3>{redirectConfig.appTitle}</h3>
                <p>{redirectConfig.appSubtitle}</p>
                <a className="appStore ios web" href={redirectConfig.appStoreLink}/>
                <a className="appStore android" href={redirectConfig.playStoreLink}/>
                {this.getStyles(redirectConfig.backgroundImage)}
            </main>
        );
    }
}