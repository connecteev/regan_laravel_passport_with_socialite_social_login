import store from '../store';

module.exports = {
    setError(error){
        store.dispatch({
            type: 'ERROR',
            payload: {
                error
            }
        });
    },
    setLoading(loading){
        store.dispatch({
            type: 'LOADING',
            payload: {
                loading
            }
        });
    },
    setMember(member){
        store.dispatch({
            type: 'MEMBER',
            payload: {
                member
            }
        });
    }
};