import store from '../store';

export const USER_REGISTER        = 'USER_REGISTER';
export const USER_LOGIN           = 'USER_LOGIN';
export const USER_SET             = 'USER_SET';
export const USER_LOGOUT          = 'USER_LOGOUT';
export const USER_REGISTER_FAILED = 'USER_REGISTER_FAILED';
export const USER_LOGIN_FAILED    = 'USER_LOGIN_FAILED';

export const register = () => ({
  type: USER_REGISTER
});

export const login = () => ({
  type: USER_LOGIN
});

export const setUser = (payload) => ({
  type: USER_SET,
  payload
});

export const logout = () => ({
  type: USER_LOGOUT
});

export const registerFailed = (payload) => ({
  type: USER_REGISTER_FAILED,
  payload,
});

export const loginFailed = (payload) => ({
  type: USER_LOGIN_FAILED,
  payload,
});
