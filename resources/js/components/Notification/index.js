import React from 'react';
import { connect } from 'react-redux';
import NotificationItem from './NotificationItem';

class Notification extends React.Component {
    render() {
        const notifications = this.props.config.notification;
        return (
            <div className="notificationContainer">
                {
                    notifications.map(function(notification, index){
                        return(
                            <NotificationItem key={index} config={notification}/>
                        )
                    })
                }
            </div>
        )
    }
}

function mapStateToProps(store) {
    return {
        config: store.notification.toJS()
    };
}

export default connect(mapStateToProps)(Notification);