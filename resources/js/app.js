import UIkit  from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

// loads the Icon plugin
UIkit.use(Icons);

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import configureStore from './store';
import Routes from './routes';
import helpers from './helpers';

import Notification from './components/Notification';

render(
    <Provider store={configureStore()}>
        <BrowserRouter>
            <section className={'App ' + helpers.getBrowserType()}>
                <Route component={Routes} />
                <Notification/>
            </section>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app')
);

