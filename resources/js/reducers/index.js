import { combineReducers } from 'redux';

// Reducers
import app from './app';
import notification from './notification';

// Combine Reducers
var reducers = combineReducers({
    app,
    notification
});

export default reducers;
