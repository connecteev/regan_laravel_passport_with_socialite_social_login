import immutable from 'immutable';
const initialState = immutable.Map({
    notification:immutable.List([])
});

function controlReducer(state = initialState, action) {
    switch(action.type) {
        case 'NOTIFICATION_ADD':
            return state.updateIn(['notification'], list => list.push(action.notification));

        case 'NOTIFICATION_REMOVE':
            return state.updateIn(['notification'], list => list.filter(n => n.id !== action.id));
    }

    return state;
}

export default controlReducer;
