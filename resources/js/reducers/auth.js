import immutable from 'immutable';

import {
  USER_REGISTER,
  USER_LOGIN,
  USER_SET,
  USER_LOGOUT,
  USER_REGISTER_FAILED,
  USER_LOGIN_FAILED,
} from '../actions/auth';

const initialState = immutable.Map({
  loading: true,
  user: false,
  error: null
});

function appReducer(state = initialState, action) {
    switch(action.type) {
        case USER_REGISTER:
            return state.merge({
                loading: true,
                error: null,
            });

        case USER_LOGIN:
            return state.merge({
                loading: true,
                error: null,
            });

        case USER_SET:
            return state.merge({
                loading: false,
                user : action.payload
            });

        case USER_LOGOUT:
            return state.merge({
                user: null,
                error: null,
            });

        case USER_REGISTER_FAILED:
            return state.merge({
                loading: false,
                error: action.payload
            });

        case USER_LOGIN_FAILED:
            return state.merge({
                loading: false,
                error: action.payload
            });
    }
    return state;
}

export default appReducer;
