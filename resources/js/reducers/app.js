import immutable from 'immutable';

const initialState = immutable.Map({
    loading: true,
    error: false,
});

function appReducer(state = initialState, action) {
    switch(action.type) {
        case 'ERROR':
            return state.merge({
                loading: false,
                error : action.payload.error
            });

        case 'LOADING':
            return state.merge({
                loading: action.payload.loading
            });
    }
    return state;
}

export default appReducer;
