import {fk, many, attr, Model} from 'redux-orm';

class User extends Model {
  toString() {
    return `${this.firstName}`;
  }
  // Declare any static or instance methods you need.
}
Book.modelName = 'User';

// Declare your related fields.
Book.fields = {
  id: attr(), // non-relational field for any value; optional but highly recommended
  firstName: attr(),
  lastName: attr(),
  username: attr(),
  email: attr(),
  trialEndsAt: attr(),
  authors: many('Author', 'books'),
  publisher: fk('Publisher', 'books'),
};

export default Book;
