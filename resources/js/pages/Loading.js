import React from 'react';

export default class extends React.Component {
  render() {
      if(this.props.loading){
          return <section className="Loading"/>
      }
      return this.props.children
  }
}