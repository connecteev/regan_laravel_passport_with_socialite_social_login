import React from 'react';

import FacebookLogin from 'react-facebook-login';

export default class extends React.Component {

  constructor(props) {
    super(props)

    this.responseFacebook = this.responseFacebook.bind(this);
  }

  responseFacebook(payload) {
    if (payload.accessToken) {
      let url = `https://obxai.lndo.site/auth/social/facebook?accessToken=${payload.accessToken}`;

      fetch(url, {
        // method: 'post',
        // body: payload
      }).then(function(response) {
        return response.json();
      }).then(function(data) {
        console.log(data);
      });
    }

  }

  render() {
    let facebookId = '2310389135649557';

    return (
      <React.Fragment>
        <img data-srcset="https://picsum.photos/640/700/?image=1044 640w,
		             https://picsum.photos/960/700/?image=1044 960w,
		             https://picsum.photos/1200/900/?image=1044 1200w,
		             https://picsum.photos/2000/1000/?image=1044 2000w"
             sizes="100vw"
             data-src="https://picsum.photos/1200/900/?image=1044" alt="" data-uk-cover="" data-uk-img=""
        />

          <div className="uk-position-cover uk-overlay-primary"></div>

          <div
            className="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-light uk-position-relative uk-position-z-index">
            <div className="uk-position-bottom-center uk-position-small uk-visible@m">
              <span className="uk-text-small uk-text-muted">© 2019 Company Name - <a
                href="https://github.com/zzseba78/Kick-Off">Created by KickOff</a> | Built with <a
                href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span
                data-uk-icon="uikit"></span></a></span>
            </div>
            <div className="uk-width-medium uk-padding-small" uk-scrollspy="cls: uk-animation-fade">
              <div className="uk-text-center uk-margin">
                <img src="/img/logo/obx-white.png" alt="OBX.ai" />
              </div>
              <form action="login-dark.html">
                <fieldset className="uk-fieldset">
                  <div className="uk-margin">
                    <div className="uk-inline uk-width-1-1">
                      <span className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
                      <input className="uk-input uk-border-pill" required placeholder="Username" type="text" />
                    </div>
                  </div>
                  <div className="uk-margin">
                    <div className="uk-inline uk-width-1-1">
                      <span className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
                      <input className="uk-input uk-border-pill" required placeholder="Password" type="password" />
                    </div>
                  </div>

                  <div className="uk-margin">
                    <label><input className="uk-checkbox" type="checkbox" /> Keep me logged in</label>
                  </div>
                  <div className="uk-margin">
                    <button type="submit" className="uk-button uk-button-primary uk-border-pill uk-width-1-1">LOG IN
                    </button>
                  </div>
                  <div className="uk-margin">
                    <FacebookLogin
                      appId={facebookId}
                      autoLoad={true}
                      fields="name,email,picture,gender,verified,link"
                      callback={this.responseFacebook}
                    />
                  </div>
                </fieldset>
              </form>
              <div>
                <div className="uk-text-center">
                  <a className="uk-link-reset uk-text-small"
                     data-uk-toggle="target: #recover;animation: uk-animation-slide-top-small">Forgot your password?
                  </a>
                </div>
                <div className="uk-margin-small-top" id="recover" hidden>
                  <form action="login-dark.html">
                    <div className="uk-margin-small">
                      <div className="uk-inline uk-width-1-1">
                        <span className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
                        <input className="uk-input uk-border-pill" placeholder="E-mail" required type="text" />
                      </div>
                    </div>
                    <div className="uk-margin-small">
                      <button type="submit" className="uk-button uk-button-primary uk-border-pill uk-width-1-1">SEND
                        PASSWORD
                      </button>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
      </React.Fragment>
    );
  }
}
