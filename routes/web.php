<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', ['as'=>'login', function() {
    $message = [
        'config' => '{}',
        'caching' => env('APP_ENV') !== 'local'
    ];

    return view('app', $message);
}]);

Route::any('/auth/social/facebook', 'SocialLoginController@loginFacebook');
Route::any('/auth/social/issue', 'SocialLoginController@issueToken');

//Route::post('/auth/social/facebook', function () {
//
//    dd('SocialLoginController@loginFacebook');
//});

Route::get('/', function (Request $request)  {

    $message = [
        'config' => '{}',
        'caching' => env('APP_ENV') !== 'local'
    ];

    return view('app', $message);
});

Route::get('/{any}', function ($any) {

    $message = [
        'config' => '{}',
        'caching' => env('APP_ENV') !== 'local'
    ];

    return view('app', $message);
})->where('any', '.*');
