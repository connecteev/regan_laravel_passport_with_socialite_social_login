# Features

- [] Recurring Reviews (https://github.com/faustbrian/Laravel-Recurring)
- [] Teams
    - [] Option #1 (https://github.com/mpociot/teamwork#inviting-others)
    - [] Option #2 (https://packalyst.com/packages/package/ffogarasi/laravel-roles)
- [] Billing (Stripe / Cashier)
- [] Survey Bundle ??? (https://packagist.org/packages/mcesar/laravel-survey)
- [] Custom attribute data on models ??? (https://packalyst.com/packages/package/rinvex/laravel-attributes)
- [] Activity logging (https://github.com/spatie/laravel-activitylog)
- [] Auditing / Model history / VCS ??? (https://packalyst.com/packages/package/owen-it/laravel-auditing)
- [] Friends ??? (https://packalyst.com/packages/package/rennokki/befriended)
- [] Single Table Inheritance (https://github.com/Nanigans/single-table-inheritance)
- [] 
- [] 
- [] 
- [] 
- []
- [] 
