<?php

namespace App\Pipeline;

use App\Models\User;
use Carbon\Carbon;
use Closure;

class ValidateDob {

    /**
     * @param User $user
     * @param Closure $next
     * @return Closure
     */
    public function handle(User $user, Closure $next) {
        try {
            if ($user->dob) {
                $user->dob = Carbon::parse($user->dob)->toDateString();
            }
        } catch (\Exception $e) {
            $user->dob = null;
        }

        return $next($user);
    }
}