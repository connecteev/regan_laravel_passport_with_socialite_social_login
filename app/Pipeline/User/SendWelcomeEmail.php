<?php

namespace App\Pipeline;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail {

    /**
     * @param User $user
     * @param Closure $next
     * @return Closure
     */
    public function handle(User $user, Closure $next) {
//        Mail::to($user->email)->send();

        return $next($user);
    }
}