<?php

namespace App\Observers;

use App\Models\User;
use App\Pipeline\ValidateDob;

class UserObserver extends AbstractObserver
{
    protected $observe = [
        'saving' => [
            ValidateDob::class,
        ],
    ];

}
