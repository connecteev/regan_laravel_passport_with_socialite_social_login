<?php

namespace App\Observers;

use App\Models\User;
use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

abstract class AbstractObserver
{
    protected $observe = [];

    /**
     * Handle the user "created" event.
     *
     * @param Model $model
     * @return void
     */
    public function created(Model $model)
    {
        $this->run('created', $model);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param Model $model
     * @return void
     */
    public function updated(Model $model)
    {
        $this->run('updated', $model);
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function deleted(Model $model)
    {
        $this->run('deleted', $model);
    }

    /**
     * Handle the user "restored" event.
     *
     * @param Model $model
     * @return void
     */
    public function restored(Model $model)
    {
        $this->run('restored', $model);
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param Model $model
     * @return void
     */
    public function forceDeleted(Model $model)
    {
        $this->run('forceDeleted', $model);
    }


    protected function run($action, Model $model)
    {
        //  Return  if  we  have  no  handlers  for  the  action.
        if (!array_key_exists($action, $this->observe)) {
            return false;
        }

        //  Run  the  pipeline.
        return resolve(Pipeline::class)
            ->send($model)
            ->through($this->observe[$action])
            ->then(function (Model $model) {
            return $model;
        });
    }
}