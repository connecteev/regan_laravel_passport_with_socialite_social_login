<?php

namespace App\Http\Controllers;

use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Route;

class SocialLoginController extends Controller {

    public function loginFacebook(Request $request) {
        try {
            /** @var \Laravel\Socialite\Two\User $facebook */
            $service = Socialite::driver('facebook')->userFromToken($request->get('accessToken'));

            if(!$exist = SocialAccount::where('provider',  SocialAccount::SERVICE_FACEBOOK)->where('provider_user_id', $service->getId())->first()){
                $email = $service->email;
                $user = User::where('email', $email)->first();

                if (!$user) {


                    $user = User::create([
                        'username' => $email,
                        'email' => $email,
                    ]);
                }

                SocialAccount::create([
                    'provider' => SocialAccount::SERVICE_FACEBOOK,
                    'provider_user_id' => $service->id,
                    'user_id' => $user->id
                ]);
            }
            return response()->json($this->issueToken($request, 'facebook', $request->accessToken));
        }
        catch(\Exception $e) {
            return response()->json([ "error" => $e->getMessage() ]);
        }
    }

    public function issueToken($request, $provider, $accessToken) {

        $clientId = env('CLIENT_KEY', '2');
        $clientSecret = env('CLIENT_SECRET', 'cwmX6xgeNEiNIYmAWCsacFJiw2jf1Thjo4mQkESS');

        /**
         * Here we will request our app to generate access token
         * and refresh token for the user using its social identity by providing access token
         * and provider name of the provider. (I hope its not confusing)
         * and then it goes through social grant and which fetches providers user id then calls
         * findForPassportSocialite from your user model if it returns User object then it generates
         * oauth tokens or else will throw error message normally like other oauth requests.
         */
        $params = [
            'grant_type' => 'social',
            'client_id' => $clientId, // it should be password grant client
            'client_secret' => $clientSecret,
            'accessToken' => $accessToken, // access token from provider
            'provider' => $provider, // i.e. facebook
            'scope' => '*',
        ];
        $request->request->add($params);

        $requestToken = Request::create("oauth/token", "POST");
        $response = Route::dispatch($requestToken);

        return json_decode((string) $response->getContent(), true);
    }
}
