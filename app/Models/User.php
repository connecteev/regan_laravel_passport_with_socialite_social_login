<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;
use Schedula\Laravel\PassportSocialite\User\UserSocialAccount;

class User extends Authenticatable implements UserSocialAccount
{
    use Uuids, Notifiable, HasApiTokens, Billable;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Find user using social provider's id
     *
     * @param string $provider Provider name as requested from oauth e.g. facebook
     * @param string $id User id of social provider
     *
     * @return User
     */
    public static function findForPassportSocialite($provider, $id) {
        $account = SocialAccount::where('provider', $provider)->where('provider_user_id', $id)->first();

        if($account && $account->user) {
            return $account->user;
        }

        return;
    }
}
