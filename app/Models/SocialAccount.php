<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    public const SERVICE_FACEBOOK = 'facebook';
    public const SERVICE_GOOGLE = 'google';
    public const SERVICE_LINKEDIN = 'linkedin';

    protected $fillable = [
        'provider',
        'provider_user_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
